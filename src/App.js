// import logo from './logo.svg';
import React from 'react';

import {
    BrowserRouter as Router,
    Switch,
    Route,
    Link
} from "react-router-dom";


const App = () => {
    return (
        <section className="App">
            <Router>
                <Link to="/">Home</Link>
                <Link to="/about">About</Link>
                <Route exact path="/" component={IndexPage} />
                <Route exact path="/users" component={UsersPage} />
                <Route path="/user/:userId" component={UserPage} />
                <Route exact path="/about" component={AboutPage} />
            </Router>
            <a href="/about">about with browser reload</a>
        </section>
    );
};

const IndexPage = () => {
    return (
        <h3>Home Page</h3>
    );
};

const AboutPage = () => {
    return (
        <h3>About Page</h3>
    );
};
const users = [
    {
      name: `Param`,
    },
    {
      name: `Vennila`,
    },
    {
      name: `Afrin`,
    },
  ];
  // userId будет индекс в массиве + 1
const UsersPage = () => {
    return (
      <>
        <h3>Users Page</h3>
        {users.map((user, index) => (
          <h5 key={index}>
            <Link to={`/user/${index + 1}`}>{user.name}'s Page</Link>
          </h5>
        ))}
      </>
    );
  };
  const UserPage = ({ match, location }) => {  return (
    <>
      <p>
        <strong>Match Props: </strong>
        <code>{JSON.stringify(match, null, 2)}</code>      
      </p>
      <p>
        <strong>Location Props: </strong>
        <code>{JSON.stringify(location, null, 2)}</code>      
      </p>
    </>
  );
};
export default App;